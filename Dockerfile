FROM amd64/debian:buster-slim

# Install the basic packages from the default repository.
RUN apt-get --yes update && \
    apt-get --yes --no-install-recommends install \
        apt-transport-https \
        ca-certificates \
        curl \
        git \
        gnupg2 \
        locales \
        pylint3 \
        python3 \
        python3-pip \
        python3-venv \
        python3-dev \
        software-properties-common \
        yapf3

# Set Python 3 as a Python alternative.
RUN update-alternatives --install /usr/bin/python python \
    /usr/bin/python3 1

# Set pip3 as a pip alternative.
RUN update-alternatives --install /usr/bin/pip pip \
    /usr/bin/pip3 1

# Set locale to en_US.UTF-8.
RUN rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 \
        -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# Install bazel.
RUN curl -fsSL https://bazel.build/bazel-release.pub.gpg | \
        apt-key add - && \
    add-apt-repository \
        "deb [arch=amd64] https://storage.googleapis.com/bazel-apt \
         stable jdk1.8" && \
    apt-get --yes update && \
    apt-get --yes --no-install-recommends install bazel

# Install Docker.
RUN apt-get --yes remove --purge --ignore-missing docker \
        docker.io runc && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | \
        apt-key add - && \
    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian \
         $(lsb_release -cs) \
         stable" && \
    apt-get --yes update && \
    apt-get --yes --no-install-recommends install docker-ce \
        docker-ce-cli containerd.io

# Install Google Cloud SDK.
RUN curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
        apt-key add - && \
    add-apt-repository \
        "deb [arch=amd64] https://packages.cloud.google.com/apt \
         cloud-sdk main" && \
    apt-get --yes update && \
    apt-get --yes --no-install-recommends install google-cloud-sdk
